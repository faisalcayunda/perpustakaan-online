# Perpustakaan Online 


## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
Before we begin, ensure that you have installed Docker on your machine. If not, you can download Docker Desktop for Mac or Windows here. For Linux, you can use the appropriate package manager to install Docker.

### Clone the Repository
Firstly, you will need to clone the repository:

```git clone https://gitlab.com/faisalcayunda/perpustakaan-online.git```
Navigate to the project directory:

```cd perpustakaan-online```

Building the Docker Image
To build the Docker image, run the following command in your terminal:

```docker build -t registry.gitlab.com/faisalcayunda/perpustakaan-onlline:main -f Dockerfile . ```


Running the Docker Container
After the Docker image has been built, you can run it with the following command:

docker run -p 8000:5555 registry.gitlab.com/faisalcayunda/perpustakaan-onlline:main
In the above command, -p 8000:8000 maps your system's port 8000 to the container's port 8000. 

Access the application via: http://localhost:8000 or http://127.0.0.1:8000 in your browser.

Built With
Docker - A tool designed to make it easier to create, deploy, and run applications by using containers.