from sqlalchemy.ext.declarative import declarative_base
from typing import List
from datetime import datetime
from typing import Self
from fastapi_sqlalchemy import db
from sqlalchemy import or_, cast, String, text


base = declarative_base()
class Base(base):
    __abstract__ = True
    
    def __repr__(self) -> str:
        return str(self.to_dict())

    def to_dict(self) -> dict:
        return {c.name: self._get_string(getattr(self, c.name)) for c in self.__table__.columns}

    def get_columns(self) -> List[str]:
        return [c.name for c in self.__table__.columns]

    def get_tablename(self) -> str:
        return self.__tablename__

    @staticmethod
    def _get_string(x):
        return x.isoformat() if isinstance(x, datetime) else x
    
    def get_list(self, where:dict, sort, search, page, limit) -> dict:
        query = db.session.query(self.__class__)
        if where:
            for key, value in where.items():
                if key in self.get_columns():
                    query = query.filter(getattr(self.__class__, key) == value)
                    
        for key in self.get_columns():
            query = query.filter(or_(cast(getattr(self.__class__, key), String).ilike(f"%{search}%")))
            
        if sort[1] == "desc":
            query = query.order_by(getattr(self.__class__, sort[0]).desc())
        else:
            query = query.order_by(getattr(self.__class__, sort[0]).asc())
            
        result = query.limit(limit).offset((page - 1) * limit)
        return {
            "data": [instance.to_dict() for instance in result.all()],
            "total": query.count()
        }
    
    def get_value(self, where:dict) -> dict or None:
        if instance := db.session.query(self.__class__).filter_by(**where).first():
            return instance.to_dict()
        else:
            return None         
        
    
    def insert_into(self, values:dict) -> dict or bool:
        for key, value in values.items():
            if key in self.get_columns():
                setattr(self, key, value)
            else:
                return False
        db.session.add(self)
        db.session.commit()
        return self.to_dict()
    
    def update(self, where:dict, values:dict) -> dict or bool:
        instance = self.get_value(where)
        if not instance:
            return False
        db.session.query(self.__class__).filter_by(**where).update(values, synchronize_session=False)        
        db.session.commit()
        return self.get_value({"id":instance["id"]})
    
    def delete(self, where:dict) -> bool:
        if not (self := self.get_value(where)):
            return False
        self.is_deleted = True
        db.session.commit()
        return True
            
                