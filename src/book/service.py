from src.book.model import Book
from src.book.depenencies import set_slug
from fastapi import HTTPException, status
from src.history_borrow.service import create_history, get_history_list
from datetime import datetime, timedelta

book = Book()

async def get_book(identity:dict):
    if result := book.get_value(identity):
        return result
    else:
        return None

async def get_book_list(where, sort, search, page, limit):
    if result := book.get_list(where, sort, search, page, limit):
        return result
    else:
        return None

async def create_book(values:dict):
    values["slug"] = set_slug(values["title"])
    if result := book.insert_into(values):
        return result
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Request Body")

async def update_book(identity:dict, values:dict):
    if not (_book := await get_book(identity)):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Book does not exist")

    if values.get("title") and values["title"] != _book["title"]:
        values["slug"] = set_slug(values["title"])
        if current := await get_book({"slug": values["slug"]}):
            if len(current) > 1:
                values["slug"] = values["slug"] + "-" + str(len(current)+1)
                
    if result := book.update(identity, values):
        return result
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Request Body or check if the book already exists")

async def delete_book(identity:dict):
    if not await get_book(identity):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Book does not exist")
    if result := book.delete(identity):
        return result
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot delete book")
    
async def borrow_book(where:dict, user_id:str):
    if not (_book := await get_book(where)):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Book does not exist")
    if _book["book_qty"] == 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Book is not available")
    if history := get_history_list({"user_id": user_id, "status": "borrowed"}, "created_at:desc", "", 1, 1):
        if datetime.now() - history[0]["created_at"] < timedelta(days=7):
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="User is still borrowed a book")
    if result := book.update(where, {"book_qty": _book["book_qty"]-1}):
        await create_history({"book_slug": _book["id"], "user_id": user_id, "status": "borrowed"})
        return result
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot borrow book")
    
async def return_book(where:dict, user_id:str):
    if not (_book := await get_book(where)):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Book does not exist")
    notes = ""
    history = get_history_list({"book_slug": _book["id"], "user_id": user_id, "status": "borrowed"}, "created_at:desc", "", 1, 1)
    if not history:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="History borrowed not found")
    if datetime.now() > history[0]["created_at"] + timedelta(days=7):
        notes = "Book is overdue"
    if not (history := await create_history({"book_id": _book["id"], "user_id": user_id, "status": "returned", "notes": notes})):
        raise Exception("Cannot create history")
    if result := book.update(where, {"book_qty": _book["book_qty"]+1}):
            return result
    else:
        raise Exception("Error when return book")