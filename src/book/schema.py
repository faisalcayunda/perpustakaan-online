from pydantic import BaseModel


class BookSchema(BaseModel):
    title: str
    author: str = ""
    publisher: str = ""
    book_qty: int = 1
    notes: str = ""
    