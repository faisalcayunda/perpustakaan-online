from fastapi import APIRouter, Depends, HTTPException, status
from src.book.schema import BookSchema
from src.book.service import get_book, get_book_list, create_book, update_book, delete_book, borrow_book, return_book
import json
from src.utils import Response
from src.user.service import get_user
from fastapi import Request
from src.auth.dependencies import decode_token
from src.auth.service import get_current_active_user
from typing import Annotated
from src.auth.schema import UserSchema


router = APIRouter(
    prefix="/book",
    tags=["book"],
)

@router.get("/{slug}", status_code=status.HTTP_200_OK)
async def get_book_api(slug:str, current_user : Annotated[UserSchema, Depends(get_current_active_user)], purpose:str = "view" ):
    user_id = current_user.id
    match purpose:
        case "view":
            if book := get_book({"slug": slug}):
                return Response.success(data=book, message="Book found")
            else:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Book not found")
        case "borrow":
            if book := await borrow_book({"slug": slug}, user_id):
                return Response.success(data=book, message="Book borrowed")
            else:
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="Error while borrowing book")
        case "return":
            if book := await return_book({"slug": slug}, user_id):
                return Response.success(data=book, message="Book returned")
            else:
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="Error while returning book")
            

@router.get("/", status_code=status.HTTP_200_OK)
async def get_book_list_api(
    where:str = "{}",
    sort:str = "created_at:desc",
    search:str = "",
    page:int = 1,
    limit:int = 100,
):
    where = json.loads(where)
    sort = sort.split(":")

    if book := await get_book_list(where, sort, search, page, limit):
        return Response.success(data=book, message="Book list found")
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No book list found")

@router.post("/", status_code=status.HTTP_201_CREATED)
async def create_book_api(book:BookSchema):
    if book := await create_book(book.dict()):
        return Response.success(data=book, message="Book created")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Request Body")

@router.put("/{slug}", status_code=status.HTTP_200_OK)
async def update_book_api(slug:str, book:BookSchema):
    if book := await update_book({"slug": slug}, book.dict()):
        return Response.success(data=book, message="Book updated")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Request Body or check if the book already exists")

@router.delete("/{slug}", status_code=status.HTTP_200_OK)
async def delete_book_api(slug:str):
    if book := await delete_book({"slug": slug}):
        return Response.success(data=book, message="Book deleted")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot delete book")
    
