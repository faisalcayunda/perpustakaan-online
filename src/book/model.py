from src.models import Base
import uuid
from sqlalchemy import Column, String, DateTime, func, Boolean, Integer

class Book(Base):
    __tablename__ = "book"
    
    id = Column(String(255), primary_key=True, default=lambda: str(uuid.uuid4()), nullable=False)
    title = Column(String(255), nullable=False)
    slug = Column(String(255), nullable=False)
    author = Column(String(255))
    publisher = Column(String(255))
    book_qty = Column(Integer, default=1)
    notes = Column(String(255))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
    
    def delete(self, where:dict) -> bool:
        if not (book := self.get_value(where)):
            return False
        self.update({"id":book["id"]}, {"is_deleted":True})
        return True