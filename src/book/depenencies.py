import re


def set_slug(title:str) -> str:
    title = title.lower()
    title = title.strip()
    title = re.sub("[^a-z0-9]", "-", title)
    return title