from pydantic import BaseSettings
from functools import lru_cache

class Config(BaseSettings):
    APP_NAME: str
    PORT: int
    DEBUG: bool
    RELOAD: bool
    WORKER: int
    
    ALGORITHM: str
    SECRET_KEY: str

    DB_DRIVER: str
    DB: str
    DB_HOST: str
    DB_PORT: int
    DB_USER: str
    DB_PASSWORD: str
    
    @property
    def DB_URL(self):
        return f"{self.DB_DRIVER}://{self.DB_USER}:{self.DB_PASSWORD}@{self.DB_HOST}:{self.DB_PORT}/{self.DB}"

    SQLALCHEMY_ECHO: bool
    SQLALCHEMY_POOL_SIZE: int
    SQLALCHEMY_POOL_PRE_PING: bool
    SQLALCHEMY_POOL_TIMEOUT: int
    SQLALCHEMY_MAX_OVERFLOW: int
    SQLALCHEMY_POOL_RECYCLE: int
    SQLALCHEMY_RECORD_QUERIES: bool
    
    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        
@lru_cache()
def get_config():
    return Config()


config = get_config()