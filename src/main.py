from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from src.exceptions import internal_error_handler, validation_error_handler
from fastapi_sqlalchemy import DBSessionMiddleware
from src.auth.router import router as auth_router
from src.user.router import router as user_router
from src.book.router import router as book_router
from src.history_borrow.router import router as history_borrow_router
from src.config import config


def configure_middleware(app:FastAPI):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"]
    )
    app.add_middleware(DBSessionMiddleware, db_url=config.DB_URL)
    
def configure_handlers(app:FastAPI):
    app.add_exception_handler(Exception, internal_error_handler)
    app.add_exception_handler(RequestValidationError, validation_error_handler)
    
def configure_routers(app:FastAPI):
    app.include_router(auth_router, tags=["auth"])
    app.include_router(user_router, tags=["user"])
    app.include_router(book_router, tags=["book"])
    app.include_router(history_borrow_router, tags=["history_borrow"])

def configure_app(app:FastAPI):
    app.mount("/static", StaticFiles(directory="static"), name="static")
    configure_middleware(app)
    configure_handlers(app)
    configure_routers(app)
    
def init_app():
    app:FastAPI = FastAPI(
        debug=config.DEBUG,
        title=config.APP_NAME,
        version="1.0.0",
        description="Perpustakaan Online API",
        docs_url="/",
    )
    configure_app(app)
    
    return app

