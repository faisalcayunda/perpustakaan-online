from fastapi.responses import JSONResponse
from fastapi import HTTPException, status

class Response():
    
    @staticmethod
    def success(data:dict, message=""):
        return JSONResponse(
            status_code=200,
            content={
                "data": data,
                "message": message
            },
            media_type="application/json",
            headers={"Access-Control-Allow-Origin": "*"}
        )
    @staticmethod
    def error(message, status_code=400):
         return JSONResponse(
            status_code=status_code,
            content={
                "message": message
            },
            media_type="application/json",
            headers={"Access-Control-Allow-Origin": "*"}
        )
    
    @staticmethod
    def not_found(message="Not Found"):
        message = {"message": message}
        return JSONResponse(
            status_code=404,
            content=message,
            media_type="application/json"
        )
    
    @staticmethod
    def unauthorized(message="Unauthorized"):
        return JSONResponse(
            status_code=401,
            content={
                "message": message
            },
            media_type="application/json"
        )