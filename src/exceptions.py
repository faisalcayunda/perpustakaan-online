from fastapi import HTTPException, Request
from fastapi.exceptions import RequestValidationError
from .utils import Response

    
def internal_error_handler(request: Request, exc: Exception):
    return Response.error(message=str(exc), status_code=500)

def validation_error_handler(request: Request, exc: RequestValidationError):
    return Response.error(message=exc.errors(), status_code=400)