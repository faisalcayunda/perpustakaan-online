from src.utils import Response
from src.user.model import User
from src.auth.dependencies import get_password_hash
from fastapi import HTTPException, status

user: User = User()

async def get_user(identity:str):
    if result := user.get_value(identity):
        return result
    else:
        return None

async def get_user_list(where, sort, search, page, limit):
    if result := user.get_list(where, sort, search, page, limit):
        return result
    else:
        return None

async def create_user(values:dict):
    if email := await get_user({"email": values["email"]}):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Email already exists")
    email = values["email"]
    email = email.split("@")
    if len(email) != 2 or email[1] not in ["gmail.com", "yahoo.com", "outlook.com"]:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Email")
    password = values["password"]
    if len(password) < 8:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Password must be at least 8 characters")
    else:
        is_upper = []
        for char in password:
            if char.isupper():
                is_upper.append(True)
            else:
                continue
        if not is_upper:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Password must contain at least 1 uppercase character")
    values["password"] = get_password_hash(password)
            
    if result := user.insert_into(values):
        result = await get_user({"id": result})
        return result
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Parameters")

async def update_user(identity:dict, values:dict):
    if not await get_user(identity):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="User does not exist")
    if values.get("password"):
        password = values["password"]
        if len(password) < 8:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Password must be at least 8 characters")
        else:
            is_upper = []
            for char in password:
                if char.isupper():
                    is_upper.append(True)
                else:
                    continue
            if not is_upper:
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Password must contain at least 1 uppercase character")
    if values.get("email"):
        email = values["email"]
        email = email.split("@")
        if len(email) != 2 or email[1] not in ["gmail.com", "yahoo.com", "outlook.com"]:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Email")
    if result := user.update(identity, values):
        return result
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Parameters")

async def delete_user(where:dict):
    if user.delete(where):
        return True
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Parameters")