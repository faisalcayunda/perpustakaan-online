from fastapi import APIRouter, Depends, HTTPException, status
from src.user.schema import User
from src.user.service import get_user, create_user, update_user, delete_user, get_user_list
from src.utils import Response
import json

router = APIRouter(
    prefix="/user",
    tags=["user"],
)

@router.get("/{email}", response_model=User, status_code=status.HTTP_200_OK)
async def user(email:str):
    email = {"email": email}
    if user := await get_user(email):
        return Response.success(data=user, message="Success")
    else:
        return Response.not_found(message="User Not Found")
    
@router.get("/", response_model=User, status_code=status.HTTP_200_OK)
async def user_list(where:str = "{}", sort:str="created_at:desc", search:str="", page:int=1, limit:int=100):
    where = json.loads(where)
    sort = sort.split(":")
    if users := await get_user_list(where, sort, search, page, limit):
        return Response.success(data=users["data"], message="Success")
    else:
        return Response.not_found(message="User List Empty")
    

@router.post("/", response_model=User, status_code=status.HTTP_201_CREATED)
async def create(user:User):
    if result := await create_user(user.dict()):
        return Response.success(data=result, message="Success")
    else:
        return Response.error(message="Invalid Parameters")

@router.put("/{email}", response_model=User, status_code=status.HTTP_200_OK)
async def update(email:str, user:User):
    email = {"email": email}
    if result := await update_user(email,user.dict()):
        return Response.success(data=result, message="Success")
    else:
        return Response.error(message="Invalid Parameters")

@router.delete("/{email}", response_model=User, status_code=status.HTTP_200_OK)
async def delete(email:str):
    email = {"email": email}
    if result := await delete_user(email):
        return Response.success(data=result, message="Success")
    else:
        return Response.error(message="Invalid Parameters")

        
        
    
    