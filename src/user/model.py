from src.models import Base
from sqlalchemy import Column, DateTime, String, Boolean, or_, String
from fastapi_sqlalchemy import db
import uuid
from sqlalchemy import func

class User(Base):
    __tablename__ = "user"
    
    id = Column(String(255), primary_key=True, default=lambda: str(uuid.uuid4()), nullable=False)
    firstname = Column(String(100), nullable=False)
    lastname = Column(String(100), nullable=False)
    username = Column(String(50), nullable=False, unique=True)
    password = Column(String(50), nullable=False)
    email = Column(String(50), nullable=False, unique=True)
    role = Column(String(50), nullable=False)
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())
    
    
    def delete(self, identity:str) -> bool:
        if not (user := self.get_value(identity)):
            return False
        self.update({"id":user["id"]}, {"is_deleted":True})
        return True
    