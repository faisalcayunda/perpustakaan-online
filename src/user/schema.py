from pydantic import BaseModel, EmailStr

class User(BaseModel):
    firstname: str
    lastname: str
    username: str
    password: str
    email: EmailStr
    role: str
    is_active: bool
    is_deleted: bool
    