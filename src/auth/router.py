from fastapi import APIRouter, Depends, HTTPException, status
from src.auth.schema import Token, Login
from src.auth.service import authenticate_user
from src.auth.dependencies import create_access_token
from datetime import timedelta

router = APIRouter(
    prefix="/auth",
    tags=["auth"],
)

@router.post("/login", response_model=Token, status_code=status.HTTP_200_OK)
async def login(form_data: Login):
    user = await authenticate_user(form_data.email, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=15)
    access_token = create_access_token(
        data={"sub": user["email"]}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

