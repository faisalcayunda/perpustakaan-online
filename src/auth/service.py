from typing import Annotated
from fastapi import Depends,  HTTPException, status
from jose import JWTError, jwt
from src.config import config
from src.auth.dependencies import oauth2_scheme, verify_password
from src.auth.schema import  TokenData, UserSchema
from src.user.service import get_user as get_user
    
async def authenticate_user(email: str, password: str):
    email = email.split("@")
    if len(email) == 2:
        if email[1] not in ["gmail.com", "yahoo.com", "hotmail.com"]:
            raise HTTPException(status_code=400, detail="Invalid email type")
    else:
        raise HTTPException(status_code=400, detail="Insert a valid email")
    email = "@".join(email)
    if len(password) < 8:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Password must be at least 8 characters")
    else:
        is_upper = []
        for char in password:
            if char.isupper():
                is_upper.append(True)
            else:
                continue
        if not is_upper:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Password must contain at least 1 uppercase character")
    if user := await get_user({"email":email}):
        return user if verify_password(password, user["password"]) else False
    else:
        return False

async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, config.SECRET_KEY, algorithms=[config.ALGORITHM])
        email: str = payload.get("sub")
        if email is None:
            raise credentials_exception
        token_data = TokenData(email=email)
    except JWTError:
        raise credentials_exception
    user = await get_user({"email":token_data.email})
    user.pop("password")
    return user

async def get_current_active_user(
    current_user: Annotated[UserSchema, Depends(get_current_user)]
):
    if current_user.is_active is False:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user