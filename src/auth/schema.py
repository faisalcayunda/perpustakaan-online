from pydantic import BaseModel, EmailStr
from src.user.schema import User
from typing import Annotated
from fastapi import Form

class Login(BaseModel):
    email: Annotated[EmailStr, Form(...)]
    password: Annotated[str, Form(...)]
    
class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: str | None = None
    email : str | None = None
    
class UserInDB(User):
    hashed_password: str
    
    
class UserSchema(BaseModel):
    id: str
    firstname: str
    lastname: str
    username: str
    email: EmailStr
    role: str
    is_active: bool
    is_deleted: bool
    created_at: str
    updated_at: str