from src.models import Base
from sqlalchemy import Column, String, DateTime, func
import uuid

class HistoryBorrowModel(Base):
    __tablename__ = "history_borrow"
    
    id = Column(String(255), primary_key=True, default=lambda: str(uuid.uuid4()), nullable=False)
    book_slug = Column(String(255), nullable=False)
    user_id = Column(String(255), nullable=False)
    status = Column(String(255))
    notes = Column(String(255))
    created_at = Column(DateTime, default=func.now())
    