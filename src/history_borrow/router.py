from fastapi import HTTPException, status, APIRouter
from src.history_borrow.service import get_history_list, history_delete, create_history
from src.utils import Response
import json

router = APIRouter(
    prefix="/history_borrow",
    tags=["history"],
)

@router.get("/", status_code=status.HTTP_200_OK)
async def get_history_list_api(
    where:str = "{}",
    sort:str = "created_at:desc",
    search:str = "",
    page:int = 1,
    limit:int = 100,
):
    where = json.loads(where) if where else {}
    sort = sort.split(":")

    if history := await get_history_list(where, sort, search, page, limit):
        return Response.success(data=history, message="History list found")
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No history list found")
    
@router.post("/", status_code=status.HTTP_201_CREATED)
async def create_history_api(history:dict):
    if history := await create_history(history):
        return Response.success(data=history, message="History created")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid Request Body")
    
@router.delete("/{id}", status_code=status.HTTP_200_OK)
async def delete_history_api(id:str):
    if history := await history_delete({"id": id}):
        return Response.success(data=history, message="History deleted")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot delete history")