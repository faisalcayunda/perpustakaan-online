from src.history_borrow.models import HistoryBorrowModel

history_borrow = HistoryBorrowModel()

async def get_history_list(where, sort, search, page, limit):
    if result := history_borrow.get_list(where, sort, search, page, limit):
        return result
    else:
        return None
    
async def history_delete(where):
    if result := history_borrow.delete(where):
        return result
    else:
        return False

async def create_history(values):
    if result := history_borrow.insert_into(values):
        return result
    else:
        return False