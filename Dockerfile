FROM python:3.11-slim as base

WORKDIR /app

RUN apt-get update && \
    apt-get install -y --no-install-recommends locales locales-all build-essential gcc libpq-dev libcbor0 libedit2 \
    libfido2-1 libxext6 libxmuu1 xauth python3-dev python3-venv python3-virtualenv python3-wheel && \
    rm -rf /var/lib/apt/lists/*

RUN python -m pip install --upgrade pip

COPY . /app

RUN pip install --no-cache-dir -r ./requirements.txt

RUN chmod +x ./perpustakaan.sh

EXPOSE 5555

ENTRYPOINT ["/bin/sh", "-c", "./perpustakaan.sh"]
