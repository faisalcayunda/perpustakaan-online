"""migration

Revision ID: 7a9350e78576
Revises: 
Create Date: 2023-06-08 08:50:15.369953

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7a9350e78576'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
